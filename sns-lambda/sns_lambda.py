def lambda_handler(event, context):
    print(f'Received event: {event}')
    print(f'Received {len(event["Records"])} records')
    for record in event['Records']:
        subject = record['Sns']['Subject']
        message = record['Sns']['Message']
        print('Subject:', subject)
        print('Message:', message)
        if message.startswith('error'):
            raise Exception(f'Received an error')
    return 'Success!'
