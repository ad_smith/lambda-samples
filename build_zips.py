import shutil


shutil.make_archive('bin/dynamodb-lambda', 'zip', 'dynamodb-lambda')
shutil.make_archive('bin/sqs-lambda', 'zip', 'sqs-lambda')
shutil.make_archive('bin/s3-lambda', 'zip', 's3-lambda')
shutil.make_archive('bin/sns-lambda', 'zip', 'sns-lambda')
shutil.make_archive('bin/cloudwatch-lambda', 'zip', 'cloudwatch-lambda')
