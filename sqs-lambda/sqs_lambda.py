import json


def lambda_handler(event, context):
    print("Received event", event)
    print(f'Received {len(event["Records"])} records')
    for record in event['Records']:
        body = record['body']
        print(body)
        if (body.startswith('error')):
            raise Exception(f'Received an error')
    return "Success!"
