data "aws_iam_policy_document" "sns_policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_role" "sns_role" {
  name               = "sns-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

resource "aws_iam_role_policy" "sns_role_policy" {
  name   = "sns-role-policy"
  role   = aws_iam_role.sns_role.name
  policy = data.aws_iam_policy_document.sns_policy.json
}
