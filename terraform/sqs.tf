resource "aws_sqs_queue" "queue" {
  name                      = "sample-queue"
  receive_wait_time_seconds = 20
  kms_master_key_id         = "alias/aws/sqs"
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.dlq.arn
    maxReceiveCount     = 2
  })

}

resource "aws_sqs_queue" "dlq" {
  name                      = "sample-dlq"
  receive_wait_time_seconds = 20
  kms_master_key_id         = "alias/aws/sqs"
}
