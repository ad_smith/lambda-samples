data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_role" "s3_role" {
  name               = "s3-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

resource "aws_iam_role_policy" "s3_role_policy" {
  name   = "s3-role-policy"
  role   = aws_iam_role.s3_role.name
  policy = data.aws_iam_policy_document.s3_policy.json
}
