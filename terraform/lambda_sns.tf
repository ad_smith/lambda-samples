resource "aws_lambda_function" "sns_lambda" {
  filename         = "../bin/sns-lambda.zip"
  function_name    = "sns-lambda"
  role             = aws_iam_role.sns_role.arn
  handler          = "sns_lambda.lambda_handler"
  source_code_hash = filebase64sha256("../bin/sns-lambda.zip")
  runtime          = "python3.8"
}

resource "aws_lambda_permission" "allow_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sns_lambda.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.topic.arn
}
