def lambda_handler(event, context):

    print('Received event:', event)

    for record in event['Records']:
        print('Bucket:', record['s3']['bucket']['name'])
        print('Key:', record['s3']['object']['key'])
    
    return 'Success!'
