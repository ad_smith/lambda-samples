"""
This script will put sample records in the DynamoDB table.
"""
import boto3
import random
import datetime


client = boto3.client('dynamodb')
senders = ['andrew', 'bob', 'charlie']
recipients = ['doug', 'eric', 'fred']
messages = ['hello', 'hi', 'hey', 'hola', 'hallo', 'bonjour', 'salut', 'ciao']

for i in range(10):
    item = {
        'sender_id': {
            'S': random.choice(senders)
        },
        'message_id': {
            'S': f'{random.choice(recipients)}#{datetime.datetime.now()}'
        },
        'message': {
            'S': random.choice(messages)
        }
    }
    client.put_item(
        TableName='messages',
        Item=item                    
    )
    print(f'Put {item}')
